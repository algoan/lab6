# Pipelines

Multi-purpose pipelines. Can be used to convert from one object to another using given instruction.

Sample usage:

```
        String pipeName = "name";
        int processorsAmount = 2;

        PipeLine<Obj1, Obj2> pipeLine1 = new PipeLine<>(pipeName, processorsAmount, inItem -> {
            
            Obj2 outItem;
            
            // some actions here
            
            return outItem;
        });
        
        PipeLine<Obj2, Obj3> pipeLine2 = new PipeLine<>("name2", 3, inItem -> {
            
            Obj3 outItem;
            
            // some actions here
            
            return outItem;
        });
        
        pipeLine1.setNextPipeLine(pipeLine2);
        

        pipeLine1.addItem(new Obj1());
       


```