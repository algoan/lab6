import java.util.*;
import java.util.concurrent.locks.Lock;

import static java.lang.Thread.sleep;

public class Main
{
    static  int counter = 1;
    public static void main(String[] args)
    {
        Logger.initialize();
        PipeLine<PipedObject, PipedObject> pipeLine1 = new PipeLine<>("FrameLine", 2, inItem -> {
            try
            {
                Thread.sleep(500);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            return inItem;
        });
        PipeLine<PipedObject, PipedObject> pipeLine2 = new PipeLine<>("WheelsLine",2, inItem -> {
            try
            {
                Thread.sleep(1500);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            return inItem;
        });
        PipeLine<PipedObject, PipedObject> pipeLine3 = new PipeLine<>("PedalsLine", 3, inItem -> {
            try
            {
                Thread.sleep(3000);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            return inItem;
        });
        PipeLine<PipedObject, PipedObject> pipeLine4 = new PipeLine<>("CrutchesLine", 3, inItem -> {
            try
            {
                Thread.sleep(500);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            return inItem;
        });
        pipeLine1.setNextPipeLine(pipeLine2);
        pipeLine2.setNextPipeLine(pipeLine3);
        pipeLine3.setNextPipeLine(pipeLine4);

        pipeLine1.addItem(new PipedObject("TestObject"+counter));
        counter++;
        pipeLine1.addItem(new PipedObject("TestObject"+counter));
        counter++;
        pipeLine1.addItem(new PipedObject("TestObject"+counter));
        counter++;

        /*Timer producer = new Timer();

        producer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                pipeLine1.addItem(new PipedObject("TestObject"+counter));
                counter++;
                if(counter == 7)
                {
                    producer.cancel();
                    producer.purge();
                }
            }
        },1000,3000);*/


    }
}

class PipedObject
{
    String name;
    String state;
    public PipedObject(String name)
    {
        this.name = ConsoleColors.getColor((int) (Math.random() * 6))+name+ConsoleColors.RESET;
        state = "";
    }

    public void setState(String s)
    {
        state = s;
    }

    @Override
    public String toString()
    {
        return name+" "+state;
    }
}

interface itemAddListener
{
    void onItemAdded();
}

interface taskEndedListener
{
    void onTaskEnded();
}

