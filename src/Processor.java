public abstract class Processor<E, T> implements itemAddListener, taskEndedListener
{
    private PipeLine<E, T> parent;
    private E inItem;
    private T outItem;
    private Thread runnable;

    private boolean taskEnded;
    private int pipeId;
    public Processor(PipeLine<E, T> parent, int pipeId)
    {
        this.parent = parent;
        this.runnable = new Thread();
        this.taskEnded = true;
        this.pipeId = pipeId;
    }

    synchronized private boolean isTaskEnded()
    {
        return taskEnded;
    }

    synchronized private void setTaskEnded(boolean taskEnded)
    {
        this.taskEnded = taskEnded;
    }

    @Override
    synchronized public void onItemAdded()
    {
        if(isTaskEnded())
        {
            if(parent.getQueueSize() > 0)
            {
                setTaskEnded(false);
                //item = parent.getNextItem();
                runnable = new Thread(() -> {
                    inItem = parent.getNextItem();
                    Logger.print("Started processing on pipe #" + pipeId + " of " + parent.getName() + ": " + inItem.toString());
                    outItem = process(inItem);
                    Logger.print("Finished  processing on pipe #" + pipeId + " of " + parent.getName() + ": " + inItem.toString());
                    onTaskEnded();
                });
                runnable.start();
            }
        }
    }

    abstract protected T process(E inItem);

    @Override
    public void onTaskEnded()
    {
        parent.sendTaskToNext(outItem);
        setTaskEnded(true);
        if(parent.getQueueSize() > 0)
            onItemAdded();
    }
}

