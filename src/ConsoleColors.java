public class ConsoleColors {

    // Reset
    public static final String RESET = "\033[0m";  // Text Reset

    // Regular Colors
    public static final String RED = "\033[0;31m";     // RED
    public static final String GREEN = "\033[0;32m";   // GREEN
    public static final String YELLOW = "\033[0;33m";  // YELLOW
    public static final String BLUE = "\033[0;34m";    // BLUE
    public static final String PURPLE = "\033[0;35m";  // PURPLE
    public static final String CYAN = "\033[0;36m";    // CYAN

    public static String getColor(int n)
    {
        switch (n%6)
        {
            case 1:
                return RED;

            case 2:
                return GREEN;

            case 3:
                return YELLOW;

            case 4:
                return BLUE;

            case 5:
                return PURPLE;

            case 0:
                return CYAN;

            default:
                return GREEN;
        }
    }
}
