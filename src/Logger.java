import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Date;

public class Logger
{
    private static File logFile = new File("lab6.log");
    private static File errorFile = new File("lab6.error");
    public static void initialize()
    {
        try
        {
            if(!logFile.exists())
                logFile.createNewFile();
            if(!errorFile.exists())
                errorFile.createNewFile();
            try
            {
                Files.write(logFile.toPath(),("--------------------\n").getBytes(), StandardOpenOption.APPEND);
                Files.write(errorFile.toPath(),("--------------------\n").getBytes(), StandardOpenOption.APPEND);
            } catch (IOException e)
            {
                System.err.println(e.getMessage());
            }
            System.setErr(new PrintStream(errorFile));
        } catch (IOException e)
        {
            Logger.printError(e.getMessage());
        }
    }
    public static void print(String message)
    {
        message = (new Date())+"| " + message;
        System.out.println(message);
        if(logFile != null && logFile.exists())
            try
            {
                Files.write(logFile.toPath(),(message+"\n").getBytes(), StandardOpenOption.APPEND);
            } catch (IOException e)
            {
                System.err.println(e.getMessage());
            }
    }

    public static void printError(String message)
    {
        message = (new Date())+"| " + message;
        System.err.println(message);
        if(errorFile != null && errorFile.exists())
            try
            {
                Files.write(errorFile.toPath(),(message+"\n").getBytes(), StandardOpenOption.APPEND);
            } catch (IOException e)
            {
                System.err.println(e.getMessage());
            }
    }
}
