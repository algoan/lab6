import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.concurrent.Callable;

public class PipeLine<E, T>
{
    private ArrayDeque<E> queue;
    private ArrayList<Processor> processors;

    private String name;
    private PipeLine<T,?> nextPipeLine;

    public PipeLine(String name, int processorsAmount, Callback<E, T> processCallback)
    {
        this.name = ConsoleColors.getColor((int) (Math.random() * 6))+name+ConsoleColors.RESET;
        queue = new ArrayDeque<>();
        processors = new ArrayList<>();
        for (int i =0; i < processorsAmount; i++)
        {
            processors.add(new Processor<E, T>(this, i)
            {
                @Override
                protected T process(E inItem)
                {
                    return processCallback.process(inItem);
                }
            });
        }
    }

    synchronized public void addItem(E item)
    {
        queue.add(item);
        Logger.print("Added to "+name+" queue: "+item.toString());
        notifyAboutAdd();
    }

    synchronized public E getNextItem()
    {
        return queue.pollFirst();
    }

    synchronized int getQueueSize()
    {
        return queue.size();
    }

    synchronized String getName()
    {
        return name;
    }

    private void notifyAboutAdd()
    {
        for (Processor p: processors)
        {
            p.onItemAdded();
        }
    }

    synchronized public void sendTaskToNext(T item)
    {
        if(getNextPipeLine() != null)
            this.nextPipeLine.addItem(item);
    }

    synchronized public PipeLine<T,?> getNextPipeLine()
    {
        return nextPipeLine;
    }

    synchronized public void setNextPipeLine(PipeLine<T,?> nextPipeLine)
    {
        this.nextPipeLine = nextPipeLine;
    }
}

interface Callback<E, T>
{
     T process(E inItem);
}
